﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Accelist.HttpRequestTraining.Models
{
    public class NewTransactionModel
    {
        public string UserID { get; set; }
        public string Subject { get; set; }
        public string Content { get; set; }
    }
}
