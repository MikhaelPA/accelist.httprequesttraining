﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Accelist.HttpRequestTraining.Models
{
    public class TransactionViewModel
    {
        public string TransactionID { get; set; }
        public string UserID { get; set; }
        public string Name { get; set; }
        public string Subject { get; set; }
        public string Content { get; set; }
        public string DateTime { get; set; }

    }
}
