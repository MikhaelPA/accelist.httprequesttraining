﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Accelist.HttpRequestTraining.Models
{
    public class UserViewModel
    {
        public string UserID { get; set; }
        public string Name { get; set; }
    }
}
