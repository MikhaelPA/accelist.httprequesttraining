﻿using Accelist.Entities;
using Accelist.HttpRequestTraining.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Accelist.HttpRequestTraining.Services
{
    public class TransactionDataService
    {
        private readonly TrainingDbContext _DbContext;
        public TransactionDataService(TrainingDbContext dbContext)
        {
            _DbContext = dbContext;
        }

        /// <summary>
        /// Get transaction Data(in this case subject with content) with or without userID.
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="cancellation"></param>
        /// <returns></returns>
        public async Task<List<TransactionViewModel>> GetTransactionDataAsync(string userId, string transactionId, CancellationToken cancellation)
        {
            try
            {
                var transactions = await _DbContext.TransactionDatas.Select(Q => new TransactionViewModel
                {
                    TransactionID = Q.TransactionID,
                    UserID = Q.MasterUser.UserID,
                    Name = Q.MasterUser.Name,
                    Subject = Q.Subject,
                    Content = Q.JsonData,
                    DateTime = Q.UpdatedOn.ToString("g")
                }).ToListAsync(cancellation);

                if (string.IsNullOrWhiteSpace(userId) == false)
                {
                    //Filtered by userID
                    transactions = transactions.Where(Q => Q.UserID == userId).ToList();
                }
                if (string.IsNullOrWhiteSpace(transactionId) == false)
                {
                    //Filtered by userID
                    transactions = transactions.Where(Q => Q.TransactionID == transactionId).ToList();
                }

                return transactions;
            }
            catch
            {
                return default;
            }
        }

        public async Task<(bool isSuccess, string errorMessage)> NewTransactionDataAsync(NewTransactionModel model, CancellationToken cancellation)
        {
            var isExist = await _DbContext.MasterUsers.AnyAsync(Q=>Q.UserID == model.UserID, cancellation);
            if (isExist == false)
            {
                return (false, "User not found.");
            }

            var newData = new TransactionData
            {
                TransactionID = Guid.NewGuid().ToString().ToHash(12),
                Subject = model.Subject,
                JsonData = model.Content,
                UpdatedOn = DateTime.Now,
                UpdatedBy = model.UserID
            };

            await _DbContext.TransactionDatas.AddAsync(newData, cancellation);
            try
            {
                await _DbContext.SaveChangesAsync(cancellation);
                return (true, default);
            }
            catch (Exception e)
            {
                return (false, e.ToString());
            }

        }


        public async Task<(bool isSuccess, string errorMessage)> RemoveTransactionDataAsync(string userId, string transactionId, CancellationToken cancellation)
        {
            var toDelete = await _DbContext.TransactionDatas
                .ToListAsync(cancellation);
            if(string.IsNullOrEmpty(userId) == false)
            {
                toDelete = toDelete.Where(Q => Q.UpdatedBy == userId).ToList();
            }
            if(string.IsNullOrEmpty(transactionId) == false)
            {
                toDelete = toDelete.Where(Q => Q.TransactionID == transactionId).ToList();
            }
            if (toDelete == null || toDelete.Count == 0)
            {
                return (false, "Transaction not found");
            }
            try
            {
                _DbContext.TransactionDatas.RemoveRange(toDelete);
                await _DbContext.SaveChangesAsync(cancellation);
                return (true, default);
            }
            catch (Exception e)
            {
                return (false, e.ToString());
            }
        }

    }
}
