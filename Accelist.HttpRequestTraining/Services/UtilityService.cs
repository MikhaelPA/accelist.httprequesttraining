﻿using System.Linq;
using System.Text;

namespace Accelist.HttpRequestTraining.Services
{
    public static class UtilityService
    {
        public static string ToHash(this string input, ushort hashLength)
        {
            var str = CreateMD5(input.ToLower());
            //hu9pld1krz5cq7xig4ysjt3m82bvafn6o0ew
            //2GHIJ347CDSMNO6T89ABPVWQRXY05UEFKL1Z
            //LZ5VnkhTl1cAPBvYgDJ3ryXsWo69OiRw7pEHdet8M0Nju2UIa4FbGfmKQxCqSz
            var allowedSymbols = "LZ5VnkhTl1cAPBvYgDJ3ryXsWo69OiRw7pEHdet8M0Nju2UIa4FbGfmKQxCqSz".ToCharArray();
            var hash = new char[hashLength];

            for (int i = 0; i < str.Length; i++)
            {
                hash[i % hashLength] = (char)(hash[i % hashLength] ^ str[i]);
            }

            for (int i = 0; i < hashLength; i++)
            {
                hash[i] = allowedSymbols[hash[i] % allowedSymbols.Length];
            }

            return new string(hash);
        }

        private static string CreateMD5(string input)
        {
            // Use input string to calculate MD5 hash
            //using (System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create())
            //{
            //    byte[] inputBytes = Encoding.ASCII.GetBytes(input);
            //    byte[] hashBytes = md5.ComputeHash(inputBytes);

            //    // Convert the byte array to hexadecimal string
            //    StringBuilder sb = new StringBuilder();
            //    for (int i = 0; i < hashBytes.Length; i++)
            //    {
            //        sb.Append(hashBytes[i].ToString("X2"));
            //    }
            //    return sb.ToString();
            //}

            using System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create();
            return string.Concat(md5.ComputeHash(Encoding.UTF8.GetBytes(input)).Select(x => x.ToString("x2")));
        }
    }
}
