﻿using Accelist.Entities;
using Accelist.HttpRequestTraining.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Accelist.HttpRequestTraining.Services
{
    public class MasterUserService
    {
        private readonly TrainingDbContext _DbContext;
        public MasterUserService(TrainingDbContext dbContext)
        {
            _DbContext = dbContext;
        }

        public async Task<string> GetUserIDAsync(string name, CancellationToken cancellation)
        {
            if (string.IsNullOrWhiteSpace(name))
            {
                return default;
            }
            return await _DbContext.MasterUsers
                .Where(Q => Q.Name.ToLower() == name.ToLower())
                .Select(Q => Q.UserID)
                .FirstOrDefaultAsync(cancellation);
        }

        public async Task<List<UserViewModel>> GetMasterUserAsync(CancellationToken cancellation)
        {
            return await _DbContext.MasterUsers.Select(Q=>new UserViewModel
            {
                Name= Q.Name,
                UserID = Q.UserID
            }).ToListAsync(cancellation);
        }

        public async Task<(bool isSuccess, string errorMessage)> RemoveAllMasterUserAsync(CancellationToken cancellation)
        {
            try
            {
                var toDelete = await _DbContext.MasterUsers.ToListAsync(cancellation);
                _DbContext.MasterUsers.RemoveRange(toDelete);
                await _DbContext.SaveChangesAsync(cancellation);
                return (true, default);
            }
            catch (Exception e)
            {
                return (false, e.ToString());
            }
        }

        public async Task<(bool isSuccess, string errorMessage)> RemoveMasterUserAsync(string userId, CancellationToken cancellation)
        {
            var toDelete = await _DbContext.MasterUsers.FirstOrDefaultAsync(Q => Q.UserID == userId, cancellation);
            if (toDelete == null)
            {
                return (false, "User not found.");
            }
            try
            {
                _DbContext.MasterUsers.Remove(toDelete);
                await _DbContext.SaveChangesAsync(cancellation);
                return (true, default);
            }
            catch (Exception e)
            {
                return (false, e.ToString());
            }
        }

        public async Task<(bool isSuccess, string message)> NewMasterUserAsync(string user)
        {
            if (string.IsNullOrWhiteSpace(user) == false)
            {
                var isExist = await _DbContext.MasterUsers.AnyAsync(Q => Q.Name.ToLower() == user.ToLower());
                if (isExist)
                {
                    return (false, "User already exist.");
                }
                var userId = Guid.NewGuid().ToString().ToHash(8);
                var newUser = new MasterUser
                {
                    UserID = userId,
                    Name = user
                };
                try
                {
                    _DbContext.MasterUsers.Add(newUser);

                    await _DbContext.SaveChangesAsync();
                    return (true, userId);
                }
                catch (Exception e)
                {
                    return (false, e.ToString());
                }
            }
            else return (false, "Input must not be empty.");
        }
    }
}
