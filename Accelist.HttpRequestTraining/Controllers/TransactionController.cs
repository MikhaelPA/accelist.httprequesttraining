﻿using Accelist.HttpRequestTraining.Models;
using Accelist.HttpRequestTraining.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Accelist.HttpRequestTraining.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TransactionController : ControllerBase
    {
        private readonly TransactionDataService _TransactionMan;
        public TransactionController(TransactionDataService transactionDataService)
        {
            _TransactionMan = transactionDataService;
        }


        [HttpGet]
        public async Task<IActionResult> GetTransactionAsync(string userId, string transactionId, CancellationToken cancellation)
        {
            var response = await _TransactionMan.GetTransactionDataAsync(userId, transactionId, cancellation);
            return Ok(response);
        }

        [HttpPost]
        public async Task<IActionResult> SubmitTransactionAsync([FromBody] NewTransactionModel model, CancellationToken cancellation)
        {
            if (ModelState.IsValid == false)
            {
                return BadRequest("Request must not be empty.");
            }
            var (isSuccess, errorMessage) = await _TransactionMan.NewTransactionDataAsync(model, cancellation);

            if (!isSuccess)
            {
                return BadRequest(errorMessage);
            }
            return Ok();
        }


        [HttpDelete]
        public async Task<IActionResult> DeleteTransactionAsync(string userId, string transactionId, CancellationToken cancellation)
        {
            if (ModelState.IsValid == false)
            {
                return BadRequest("Request must not be empty.");
            }
            var (isSuccess, errorMessage) = await _TransactionMan.RemoveTransactionDataAsync(userId, transactionId, cancellation);

            if (!isSuccess)
            {
                return BadRequest(errorMessage);
            }
            return Ok();
        }
    }
}
