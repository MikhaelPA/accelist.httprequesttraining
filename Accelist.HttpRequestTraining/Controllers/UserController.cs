﻿using Accelist.HttpRequestTraining.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Accelist.HttpRequestTraining.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly MasterUserService _UserMan;
        public UserController(MasterUserService userService)
        {
            _UserMan = userService;
        }

        [HttpGet]
        public async Task<IActionResult> GetUserAsync(string name, CancellationToken cancellation)
        {
            if (string.IsNullOrWhiteSpace(name))
            {
                return Ok(await _UserMan.GetMasterUserAsync(cancellation));
            }
            else
            {
                var userId = await _UserMan.GetUserIDAsync(name, cancellation);
                if (string.IsNullOrEmpty(userId))
                {
                    var (isSuccess, message) = await _UserMan.NewMasterUserAsync(name);
                    if (isSuccess == false)
                    {
                        return BadRequest(message);
                    }
                    userId = message;
                }
                return Ok(userId);
            }

        }

        /// <summary>
        /// Remove One user by UserID.
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="cancellation"></param>
        /// <returns></returns>
        [HttpDelete("{userId}")]
        public async Task<IActionResult> RemoveUser(string userId, CancellationToken cancellation)
        {
            var (isSuccess, errorMessage) = await _UserMan.RemoveMasterUserAsync(userId, cancellation);
            if (isSuccess)
            {
                return Ok();
            }
            return BadRequest(errorMessage);
        }

        [HttpDelete]
        public async Task<IActionResult> RemoveAllUser(CancellationToken cancellation)
        {
            var (isSuccess, errorMessage) = await _UserMan.RemoveAllMasterUserAsync(cancellation);
            if (isSuccess)
            {
                return Ok();
            }
            return BadRequest(errorMessage);
        }
    }
}
