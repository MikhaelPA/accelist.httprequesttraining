﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Accelist.Entities
{
    public class MasterUser
    {
        [Key]
        public string UserID { get; set; }

        [Required]
        public string Name { get; set; }

        public List<TransactionData> TransactionDatas { get; set; }
    }
}
