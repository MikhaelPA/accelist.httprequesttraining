﻿// <auto-generated />
using System;
using Accelist.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace Accelist.Entities.Migrations
{
    [DbContext(typeof(TrainingDbContext))]
    [Migration("20210129051354_Initial")]
    partial class Initial
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "5.0.2");

            modelBuilder.Entity("Accelist.Entities.MasterUser", b =>
                {
                    b.Property<string>("UserID")
                        .HasColumnType("TEXT");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnType("TEXT");

                    b.HasKey("UserID");

                    b.ToTable("MasterUser");
                });

            modelBuilder.Entity("Accelist.Entities.TransactionData", b =>
                {
                    b.Property<string>("TransactionID")
                        .HasColumnType("TEXT");

                    b.Property<string>("FK_MasterUser_TransactionData")
                        .HasColumnType("TEXT");

                    b.Property<string>("JsonData")
                        .IsRequired()
                        .HasColumnType("TEXT");

                    b.Property<string>("Subject")
                        .HasColumnType("TEXT");

                    b.Property<string>("UpdatedBy")
                        .IsRequired()
                        .HasColumnType("TEXT");

                    b.Property<DateTime>("UpdatedOn")
                        .HasColumnType("TEXT");

                    b.HasKey("TransactionID");

                    b.HasIndex("UpdatedBy");

                    b.ToTable("TransactionData");
                });

            modelBuilder.Entity("Accelist.Entities.TransactionData", b =>
                {
                    b.HasOne("Accelist.Entities.MasterUser", "MasterUser")
                        .WithMany("TransactionDatas")
                        .HasForeignKey("UpdatedBy")
                        .HasConstraintName("FK_MasterUser_TransactionData")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("MasterUser");
                });

            modelBuilder.Entity("Accelist.Entities.MasterUser", b =>
                {
                    b.Navigation("TransactionDatas");
                });
#pragma warning restore 612, 618
        }
    }
}
