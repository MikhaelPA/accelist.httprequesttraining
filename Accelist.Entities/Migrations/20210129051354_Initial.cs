﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Accelist.Entities.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "MasterUser",
                columns: table => new
                {
                    UserID = table.Column<string>(type: "TEXT", nullable: false),
                    Name = table.Column<string>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MasterUser", x => x.UserID);
                });

            migrationBuilder.CreateTable(
                name: "TransactionData",
                columns: table => new
                {
                    TransactionID = table.Column<string>(type: "TEXT", nullable: false),
                    Subject = table.Column<string>(type: "TEXT", nullable: true),
                    JsonData = table.Column<string>(type: "TEXT", nullable: false),
                    UpdatedBy = table.Column<string>(type: "TEXT", nullable: false),
                    UpdatedOn = table.Column<DateTime>(type: "TEXT", nullable: false),
                    FK_MasterUser_TransactionData = table.Column<string>(type: "TEXT", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TransactionData", x => x.TransactionID);
                    table.ForeignKey(
                        name: "FK_MasterUser_TransactionData",
                        column: x => x.UpdatedBy,
                        principalTable: "MasterUser",
                        principalColumn: "UserID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_TransactionData_UpdatedBy",
                table: "TransactionData",
                column: "UpdatedBy");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TransactionData");

            migrationBuilder.DropTable(
                name: "MasterUser");
        }
    }
}
