﻿using Microsoft.EntityFrameworkCore;

namespace Accelist.Entities
{
    public class TrainingDbContext : DbContext
    {
        public TrainingDbContext(DbContextOptions<TrainingDbContext> options) : base(options)
        {
        }

        public DbSet<MasterUser> MasterUsers { get; set; }
        public DbSet<TransactionData> TransactionDatas { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<MasterUser>().ToTable("MasterUser");

            modelBuilder.Entity<TransactionData>().ToTable("TransactionData");

            modelBuilder.Entity<TransactionData>()
                .HasOne(Q => Q.MasterUser)
                .WithMany(Q => Q.TransactionDatas)
                .OnDelete(DeleteBehavior.Cascade)
                .HasForeignKey(Q => Q.UpdatedBy)
                .HasConstraintName("FK_MasterUser_TransactionData");
        }
    }

}
