﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Accelist.Entities
{
    public class TransactionData
    {
        [Key]
        public string TransactionID { get; set; }

        public string Subject { get; set; }

        [Required]
        public string JsonData { get; set; }

        [Required]
        public string UpdatedBy { get; set; }

        public DateTime UpdatedOn { get; set; }


        [ForeignKey("FK_MasterUser_TransactionData")]
        public MasterUser MasterUser { get; set; }
    }
}
