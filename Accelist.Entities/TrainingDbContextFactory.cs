﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace Accelist.Entities
{

    public class TrainingDbContextFactory : IDesignTimeDbContextFactory<TrainingDbContext>
    {
        public TrainingDbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<TrainingDbContext>();
            builder.UseSqlite("Data Source=reference.db");
            return new TrainingDbContext(builder.Options);
        }
    }
}
